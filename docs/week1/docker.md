# Containers

This is one of our first exercises in the bootcamp, we will be using Docker to create a containerized application.


## Pre requisites

Head to [requirements](#requirements) and install docker, docker-compose.


## Container Images 🖼️

- Images are "executables" (.exe, .msi, .dmg)
- Images are usually stored in a Container Registry (like a repository)
- Images allow you to abstract and treat any app like a container.



### Building images

1. you need a Dockerfile
2. Dockerfile describes how you want to wrap your app.


```
docker image build ...
```

---
## Running containers (hello-world) 🏃‍♂️

When you want to run an image (executable) you "double click" by issuing the following command.


```
docker container run hello-world
```

---
## Buld and Run Whole LifeCycle Exercises 🏃‍♂️


```
git clone https://gitlab.com/kontinu/sre-bootcamp.git
cd sre-bootcamp

cd src/sre/docker/multi-language
```

!!! Note
    here you'll find 3 folders of different languages that are already containerized (python, c, bash)
    inspect them and try to understand them


### BUILD AND RUN PYTHON

```
cd python/
docker build -t myapp:py .
docker run -it myapp:py
```

### BUILD AND RUN C

```
cd c/
docker build -t myapp:c .
docker run -it myapp:c
```

### BUILD AND RUN BASH

```
cd bash/
docker build -t myapp:bash .
docker run -it myapp:bash

```


<br>

!!! question "Do you know any of those 3 languages?"
    You see how docker is a gem (💎)

    - it lets you abstract and treat any app like a container
    - instead of worrying on the details of the framework of the application, you care in making your app portable.
    - you RUN THEM IN THE EXACT SAME WAY.



---
