# Infrastructure as Code (IaC)


"Infrastructure as Code (IaC) is the management of infrastructure (networks, virtual machines, load balancers, and connection topology) in a descriptive model, using the same versioning as DevOps team uses for source code. Like the principle that the same source code generates the same binary, an IaC model generates the same environment every time it is applied. IaC is a key DevOps practice and is used in conjunction with continuous delivery."


Some of the good candidates are:

- Terraform *
- Ansible
- Chef
- and Pulumi



## Terraform

![](static/img/terraform-iac.png)


"Infrastructure as code (IaC) tools allow you to manage infrastructure with configuration files rather than through a graphical user interface. IaC allows you to build, change, and manage your infrastructure in a safe, consistent, and repeatable way by defining resource configurations that you can version, reuse, and share.

Terraform is HashiCorp’s infrastructure as code tool. It lets you define resources and infrastructure in human-readable, declarative configuration files, and manages your infrastructure’s lifecycle. Using Terraform has several advantages over manually managing your infrastructure:

- Terraform can manage infrastructure on multiple cloud platforms.
- The human-readable configuration language helps you write infrastructure code quickly.
- Terraform's state allows you to track resource changes throughout your deployments.
- You can commit your configurations to version control to safely collaborate on infrastructure."


## Hands-on


```
git clone https://gitlab.com/kontinu/sre-bootcamp.git
cd sre-bootcamp
```


```
cd src/sre/iac/terraform-aws
terraform init
```

```

ssh-keygen -t rsa -b 4096 -C "terraform hands-on"

# change and fill as needed

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=us-east-1


export TF_VAR_instance_name=<replaceWithYourName>
# Plan the infrastructure

terraform plan



# Apply the infrastructure

terraform apply

# Show the created infrastructure

terraform show
```
